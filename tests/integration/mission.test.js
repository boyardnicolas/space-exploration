const request = require('supertest');
const faker = require('faker');
const httpStatus = require('http-status');
const app = require('../../src/app');
const setupTestDB = require('../utils/setupTestDB');
const { userOneAccessToken, adminAccessToken } = require('../fixtures/token.fixture');
const { Mission } = require('../../src/models');
const {missionOne, insertMissions} = require("../fixtures/mission.fixture");

setupTestDB();

describe('Mission routes', () => {
  describe('POST /v1/missions', () => {
    let newMission;

    beforeEach(() => {
      newMission = {
        name: faker.name.findName(),
        description: faker.lorem.text(),
        launch_at: faker.date.past(),
        status: ['in_development', 'launched', 'ready_for_launch', 'ended'][Math.floor(Math.random() * 4)],
        spacecraft: null,
      };
    });

    test('regular users cannot create missions', async () => {
      const res = await request(app)
        .post('/v1/users')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send(newMission)
        .expect(httpStatus.UNAUTHORIZED);
    });

    test('admin can create missions', async () => {

      const res = await request(app)
        .post('/v1/missions')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(newMission)
        .expect(httpStatus.CREATED);

      expect(res.body).toEqual({
        id: expect.anything(),
        name: newUser.name,
        description: newUser.description,
        launch_at: newUser.launch_at,
        status: newUser.status,
        spacecraft: null
      });

      const dbMission = await Mission.findById(res.body.id);
      expect(dbMission).toBeDefined();
      expect(dbMission).toMatchObject({
        name: newMission.name,
        description: newMission.description,
        launch_at: newMission.launch_at,
        status: newMission.status,
        spacecraft: null
      });
    });

    test('should return 401 error if access token is missing', async () => {
      await request(app).post('/v1/missions').send(newMission).expect(httpStatus.UNAUTHORIZED);
    });

    test('should return 409 error if name is already used', async () => {
      await insertMissions([missionOne]);
      newMission.name = missionOne.name;

      await request(app)
        .post('/v1/users')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(newMission)
        .expect(httpStatus.CONFLICT);
    });
  });

  describe('GET /v1/missions/:missionId', () => {
    test('should return 200 and the user object if data is ok', async () => {
      await insertMissions([missionOne]);

      const res = await request(app)
        .get(`/v1/missions/${missionOne._id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: missionOne._id.toHexString(),
        name: missionOne.name,
        description: missionOne.description,
        launch_at: missionOne.launch_at,
        status: missionOne.status,
      });
    });
  });

  describe('DELETE /v1/missions/:missionId', () => {
    test('should return 204 if data is ok', async () => {
      await insertMissions([missionOne]);

      await request(app)
        .delete(`/v1/missions/${missionOne._id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NO_CONTENT);

      const dbMission = await Mission.findById(missionOne._id);
      expect(dbMission).toBeNull();
    });
  });

  describe('PATCH /v1/missions/:missionId', () => {
    test('should return 200 and successfully update mission if data is ok', async () => {
      await insertMissions([missionOne]);
      const updateBody = {
        name: faker.name.findName(),
      };

      const res = await request(app)
        .patch(`/v1/missions/${missionOne._id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        id: missionOne._id.toHexString(),
        name: missionOne.name,
        description: missionOne.description,
        launch_at: missionOne.launch_at,
        status: missionOne.status,
      });

      const dbMission = await Mission.findById(missionOne._id);
      expect(dbMission).toBeDefined();
      expect(dbMission).toMatchObject({ name: updateBody.name });
    });
  });
});
