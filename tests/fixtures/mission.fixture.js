const mongoose = require('mongoose');
const faker = require('faker');
const Mission = require('../../src/models/mission.model');

const missionOne = {
  _id: mongoose.Types.ObjectId(),
  name: faker.name.findName(),
  description: faker.lorem.text(),
  launch_at: faker.date.past(),
  status: ['in_development', 'launched', 'ready_for_launch', 'ended'][Math.floor(Math.random() * 4)],
  spacecraft: null,
};

const insertMissions = async (missions) => {
  await Mission.insertMany(missions);
};

module.exports = {
  missionOne,
  insertMissions,
};
