const allRoles = {
  user: ['getMissions', 'getSpacecrafts', 'getCelestialBodies'],
  admin: [
    'getUsers',
    'manageUsers',
    'getMissions',
    'createMissions',
    'manageMissions',
    'getSpacecrafts',
    'manageSpacecrafts',
    'getCelestialBodies',
    'manageCelestialBodies',
  ],
};

const roles = Object.keys(allRoles);
const roleRights = new Map(Object.entries(allRoles));

module.exports = {
  roles,
  roleRights,
};
