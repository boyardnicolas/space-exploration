const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const celestialBodySchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
      unique: true,
    },
    description: {
      type: String,
      required: true,
      trim: true,
      maxLength: 255,
    },
    type: {
      type: String,
      required: true,
    },
    discovered_at: {
      type: Date,
      required: true,
    },
    size: {
      type: Number,
    },
  },
  {
    timestamps: true,
  }
);

/**
 * Check if name is taken
 * @param {string} name - The celestial body's name
 * @returns {Promise<boolean>}
 */
celestialBodySchema.statics.isNameTaken = async function (name) {
  const celestialBody = await this.findOne({ name });
  return !!celestialBody;
};

// add plugin that converts mongoose to json
celestialBodySchema.plugin(toJSON);
celestialBodySchema.plugin(paginate);

/**
 * @typedef CelestialBody
 */
const CelestialBody = mongoose.model('CelestialBody', celestialBodySchema);

module.exports = CelestialBody;
