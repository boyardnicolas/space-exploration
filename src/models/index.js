module.exports.Token = require('./token.model');
module.exports.User = require('./user.model');
module.exports.Mission = require('./mission.model');
module.exports.Spacecraft = require('./spacecraft.model');
module.exports.CelestialBody = require('./celestialBody.model');
