const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const spacecraftSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
      unique: true,
    },
    description: {
      type: String,
      required: true,
      trim: true,
      maxLength: 255,
    },
    weight: {
      type: Number,
      required: false,
    },
    capacity: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
  }
);

spacecraftSchema.virtual('mission', {
  ref: 'Mission',
  localField: '_id',
  foreignField: 'spacecraft',
});

/**
 * Check if name is taken
 * @param {string} name - The spacecraft's name
 * @returns {Promise<boolean>}
 */
spacecraftSchema.statics.isNameTaken = async function (name) {
  const spacecraft = await this.findOne({ name });
  return !!spacecraft;
};

// add plugin that converts mongoose to json
spacecraftSchema.plugin(toJSON);
spacecraftSchema.plugin(paginate);

/**
 * @typedef Spacecraft
 */
const Spacecraft = mongoose.model('Spacecraft', spacecraftSchema);

module.exports = Spacecraft;
