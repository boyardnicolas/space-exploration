const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const statusList = ['in_development', 'launched', 'ready_for_launch', 'ended'];

const missionSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
      unique: true,
    },
    description: {
      type: String,
      required: true,
      trim: true,
      maxLength: 255,
    },
    launch_at: {
      type: Date,
      required: false,
    },
    status: {
      type: String,
      enum: statusList,
      default: 'in_development',
    },
    spacecraft: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Spacecraft',
    },
  },
  {
    timestamps: true,
  }
);

/**
 * Check if name is taken
 * @param {string} name - The mission's name
 * @returns {Promise<boolean>}
 */
missionSchema.statics.isNameTaken = async function (name) {
  const mission = await this.findOne({ name });
  return !!mission;
};

// add plugin that converts mongoose to json
missionSchema.plugin(toJSON);
missionSchema.plugin(paginate);

/**
 * @typedef Mission
 */
const Mission = mongoose.model('Mission', missionSchema);

module.exports = Mission;
