const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const celestialBodyValidation = require('../../validations/celestialBody.validation');
const celestialBodyController = require('../../controllers/celestialBody.controller');

const router = express.Router();

router
  .route('/')
  .post(
    auth('manageCelestialBodies'),
    validate(celestialBodyValidation.createCelestialBody),
    celestialBodyController.createCelestialBody
  )
  .get(
    auth('getCelestialBodies'),
    validate(celestialBodyValidation.getCelestialBodies),
    celestialBodyController.getCelestialBodies
  );

router
  .route('/:celestialBodyId')
  .get(
    auth('getCelestialBodies'),
    validate(celestialBodyValidation.getCelestialBody),
    celestialBodyController.getCelestialBody
  )
  .patch(
    auth('manageCelestialBodies'),
    validate(celestialBodyValidation.updateCelestialBody),
    celestialBodyController.updateCelestialBody
  )
  .delete(
    auth('manageCelestialBodies'),
    validate(celestialBodyValidation.deleteCelestialBody),
    celestialBodyController.deleteCelestialBody
  );

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: CelestialBodies
 *   description: Celestial Bodies management and retrieval
 */

/**
 * @swagger
 * /celestial_bodies:
 *   post:
 *     summary: Create a Celestial Body
 *     description: Only admins can create Celestial Bodies.
 *     tags: [CelestialBodies]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - name
 *               - description
 *               - discovered_at
 *               - type
 *             properties:
 *               name:
 *                 type: string
 *                 description: must be unique
 *               description:
 *                 type: string
 *                 maxLength: 256
 *               type:
 *                 type: string
 *               discovered_at:
 *                 type: Date
 *               size:
 *                  type: Number
 *             example:
 *               name: Halley's comet
 *               description: Clear your schedule! Will be back in 2061
 *               size: 5500
 *               type: Comet
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/CelestialBody'
 *       "409":
 *         $ref: '#/components/responses/DuplicateCelestialBodyName'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *
 *   get:
 *     summary: Get all Celestial Bodies
 *     tags: [CelestialBodies]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: query
 *         name: name
 *         schema:
 *           type: string
 *         description: Celestial Body name
 *       - in: query
 *         name: sortBy
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. name:asc)
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *         description: Maximum number of Celestial Bodies
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/CelestialBody'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /celestial_bodies/{id}:
 *   get:
 *     summary: Get a Celestial Body
 *     tags: [CelestialBodies]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Celestial Body id
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/CelestialBody'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   patch:
 *     summary: Update a celestial body
 *     description: Only admins can update celestial bodies.
 *     tags: [CelestialBodies]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: CelestialBody id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: must be unique
 *               description:
 *                 type: string
 *                 maxLength: 256
 *               launch_at:
 *                 type: Date
 *             example:
 *               description: Halley's Comet, Comet Halley, or sometimes simply Halley, officially designated 1P/Halley, is the only known short-period comet that is consistently visible to the naked eye from Earth, appearing every 75–79 years.
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/CelestialBody'
 *       "409":
 *         $ref: '#/components/responses/DuplicateCelestialBodyName'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   delete:
 *     summary: Delete a celestial body
 *     description: Only admins can delete celestial bodies.
 *     tags: [CelestialBodies]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Celestial Body id
 *     responses:
 *       "200":
 *         description: No content
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */
