const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const missionValidation = require('../../validations/mission.validation');
const missionController = require('../../controllers/mission.controller');

const router = express.Router();

router
  .route('/')
  .post(auth('manageMissions'), validate(missionValidation.createMission), missionController.createMission)
  .get(auth('getMissions'), validate(missionValidation.getMissions), missionController.getMissions);

router
  .route('/:missionId')
  .get(auth('getMissions'), validate(missionValidation.getMission), missionController.getMission)
  .patch(auth('manageMissions'), validate(missionValidation.updateMission), missionController.updateMission)
  .delete(auth('manageMissions'), validate(missionValidation.deleteMission), missionController.deleteMission);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Missions
 *   description: Mission management and retrieval
 */

/**
 * @swagger
 * /missions:
 *   post:
 *     summary: Create a mission
 *     description: Only admins can create missions.
 *     tags: [Missions]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - name
 *               - description
 *             properties:
 *               name:
 *                 type: string
 *                 description: must be unique
 *               description:
 *                 type: string
 *                 maxLength: 256
 *               launch_at:
 *                 type: Date
 *               status:
 *                  type: string
 *                  enum: ['in_development', 'launched', 'ready_for_launch', 'ended']
 *             example:
 *               name: find the answer about Life, the Universe, and Everything
 *               description: come back in half a million-year
 *               launch_at: 1979-10-12 00:00:00
 *               status: launched
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Mission'
 *       "409":
 *         $ref: '#/components/responses/DuplicateSpacecraftName'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *
 *   get:
 *     summary: Get all missions
 *     tags: [Missions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: query
 *         name: name
 *         schema:
 *           type: string
 *         description: Mission name
 *       - in: query
 *         name: sortBy
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. name:asc)
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *         description: Maximum number of missions
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Mission'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /missions/{id}:
 *   get:
 *     summary: Get a mission
 *     tags: [Missions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Mission id
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Mission'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   patch:
 *     summary: Update a mission
 *     description: Only admins can update missions.
 *     tags: [Missions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Mission id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: must be unique
 *               description:
 *                 type: string
 *                 maxLength: 256
 *               launch_at:
 *                 type: Date
 *             example:
 *               name: Get milk
 *               description: Should be simple
 *               launch_at: 2024-01-01 10:00:00
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Mission'
 *       "409":
 *         $ref: '#/components/responses/DuplicateMissionName'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   delete:
 *     summary: Delete a mission
 *     description: Only admins can delete missions.
 *     tags: [Missions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Mission id
 *     responses:
 *       "200":
 *         description: No content
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */
