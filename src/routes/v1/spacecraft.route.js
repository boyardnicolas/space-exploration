const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const spacecraftValidation = require('../../validations/spacecraft.validation');
const spacecraftController = require('../../controllers/spacecraft.controller');

const router = express.Router();

router
  .route('/')
  .post(auth('manageSpacecrafts'), validate(spacecraftValidation.createSpacecraft), spacecraftController.createSpacecraft)
  .get(auth('getSpacecrafts'), validate(spacecraftValidation.getSpacecrafts), spacecraftController.getSpacecrafts);

router
  .route('/:spacecraftId')
  .get(auth('getSpacecrafts'), validate(spacecraftValidation.getSpacecraft), spacecraftController.getSpacecraft)
  .patch(auth('manageSpacecrafts'), validate(spacecraftValidation.updateSpacecraft), spacecraftController.updateSpacecraft)
  .delete(auth('manageSpacecrafts'), validate(spacecraftValidation.deleteSpacecraft), spacecraftController.deleteSpacecraft);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Spacecrafts
 *   description: Spacecraft management and retrieval
 */

/**
 * @swagger
 * /spacecrafts:
 *   post:
 *     summary: Create a spacecraft
 *     description: Only admins can create spacecrafts.
 *     tags: [Spacecrafts]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - name
 *               - description
 *               - capacity
 *             properties:
 *               name:
 *                 type: string
 *                 description: must be unique
 *               description:
 *                 type: string
 *                 maxLength: 256
 *               capacity:
 *                 type: Number
 *               weight:
 *                  type: Number
 *             example:
 *               name: USS Enterprise
 *               description: You know it
 *               capacity: 5828
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Spacecraft'
 *       "409":
 *         $ref: '#/components/responses/DuplicateSpacecraftName'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *
 *   get:
 *     summary: Get all spacecrafts
 *     tags: [Spacecrafts]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: query
 *         name: name
 *         schema:
 *           type: string
 *         description: Spacecraft name
 *       - in: query
 *         name: sortBy
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. name:asc)
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *         description: Maximum number of spacecrafts
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Spacecraft'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /spacecrafts/{id}:
 *   get:
 *     summary: Get a spacecraft
 *     tags: [Spacecrafts]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Spacecraft id
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Spacecraft'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   patch:
 *     summary: Update a spacecraft
 *     description: Only admins can update spacecrafts.
 *     tags: [Spacecrafts]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Spacecraft id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: must be unique
 *               description:
 *                 type: string
 *                 maxLength: 256
 *               launch_at:
 *                 type: Date
 *             example:
 *               name: The starship Heart of Gold
 *               description: The starship Heart of Gold was the first spacecraft to make use of the Infinite Improbability Drive. The craft was stolen by then-President Zaphod Beeblebrox at the official launch of the ship, as he was supposed to be officiating the launch
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Spacecraft'
 *       "409":
 *         $ref: '#/components/responses/DuplicateSpacecraftName'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   delete:
 *     summary: Delete a spacecraft
 *     description: Only admins can delete spacecrafts.
 *     tags: [Spacecrafts]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Spacecraft id
 *     responses:
 *       "200":
 *         description: No content
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */
