const express = require('express');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const missionRoute = require('./mission.route');
const spacecraftRoute = require('./spacecraft.route');
const celestialBodyRoute = require('./celestialBody.route');
const docsRoute = require('./docs.route');
const config = require('../../config/config');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/auth',
    route: authRoute,
  },
  {
    path: '/users',
    route: userRoute,
  },
  {
    path: '/missions',
    route: missionRoute,
  },
  {
    path: '/spacecrafts',
    route: spacecraftRoute,
  },
  {
    path: '/celestial_bodies',
    route: celestialBodyRoute,
  },
];

const devRoutes = [
  // routes available only in development mode
  {
    path: '/docs',
    route: docsRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === 'development') {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;
