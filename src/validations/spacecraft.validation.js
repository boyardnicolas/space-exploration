const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createSpacecraft = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().required().max(256),
    capacity: Joi.number().positive().required(),
    weight: Joi.number().positive(),
  }),
};

const getSpacecrafts = {
  query: Joi.object().keys({
    name: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getSpacecraft = {
  params: Joi.object().keys({
    spacecraftId: Joi.string().custom(objectId),
  }),
};

const updateSpacecraft = {
  params: Joi.object().keys({
    spacecraftId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string(),
      description: Joi.string().max(256),
      capacity: Joi.number().positive(),
      weight: Joi.number().positive(),
    })
    .min(1),
};

const deleteSpacecraft = {
  params: Joi.object().keys({
    spacecraftId: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createSpacecraft,
  getSpacecrafts,
  getSpacecraft,
  updateSpacecraft,
  deleteSpacecraft,
};
