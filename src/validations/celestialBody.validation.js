const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createCelestialBody = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().required().max(256),
    type: Joi.string().required(),
    discovered_at: Joi.date().required(),
    size: Joi.number().positive(),
  }),
};

const getCelestialBodies = {
  query: Joi.object().keys({
    name: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getCelestialBody = {
  params: Joi.object().keys({
    celestialBodyId: Joi.string().custom(objectId),
  }),
};

const updateCelestialBody = {
  params: Joi.object().keys({
    celestialBodyId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string(),
      description: Joi.string().max(256),
      type: Joi.string(),
      discovered_at: Joi.date(),
      size: Joi.number().positive(),
    })
    .min(1),
};

const deleteCelestialBody = {
  params: Joi.object().keys({
    celestialBodyId: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createCelestialBody,
  getCelestialBodies,
  getCelestialBody,
  updateCelestialBody,
  deleteCelestialBody,
};
