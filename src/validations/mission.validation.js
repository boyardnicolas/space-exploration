const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createMission = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().required().max(256),
    launch_at: Joi.date(),
    status: Joi.string().valid('in_development', 'launched', 'ready_for_launch', 'ended').default('in_development'),
    spacecraft: Joi.string().custom(objectId),
  }),
};

const getMissions = {
  query: Joi.object().keys({
    name: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getMission = {
  params: Joi.object().keys({
    missionId: Joi.string().custom(objectId),
  }),
};

const updateMission = {
  params: Joi.object().keys({
    missionId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string(),
      description: Joi.string(),
      launch_at: Joi.date(),
      status: Joi.string().valid('in_development', 'launched', 'ready_for_launch', 'ended'),
    })
    .min(1),
};

const deleteMission = {
  params: Joi.object().keys({
    missionId: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createMission,
  getMissions,
  getMission,
  updateMission,
  deleteMission,
};
