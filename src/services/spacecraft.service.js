const httpStatus = require('http-status');
const { Spacecraft } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a spacecraft
 * @param {Object} spacecraftBody
 * @returns {Promise<Spacecraft>}
 */
const createSpacecraft = async (spacecraftBody) => {
  if (await Spacecraft.isNameTaken(spacecraftBody.name)) {
    throw new ApiError(httpStatus.CONFLICT, 'Name already taken');
  }
  return Spacecraft.create(spacecraftBody);
};

/**
 * Query for spacecrafts
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const querySpacecrafts = async (filter, options) => {
  const spacecrafts = await Spacecraft.paginate(filter, options);
  return spacecrafts;
};

/**
 * Get spacecraft by id
 * @param {ObjectId} id
 * @returns {Promise<Spacecraft>}
 */
const getSpacecraftById = async (id) => {
  return Spacecraft.findById(id);
};

/**
 * Update spacecraft by id
 * @param {ObjectId} spacecraftId
 * @param {Object} updateBody
 * @returns {Promise<Spacecraft>}
 */
const updateSpacecraftById = async (spacecraftId, updateBody) => {
  const spacecraft = await getSpacecraftById(spacecraftId);
  if (!spacecraft) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Spacecraft not found');
  }
  if (await Spacecraft.isNameTaken(updateBody.name)) {
    throw new ApiError(httpStatus.CONFLICT, 'Name already taken');
  }
  Object.assign(spacecraft, updateBody);
  await spacecraft.save();
  return spacecraft;
};

/**
 * Delete spacecraft by id
 * @param {ObjectId} spacecraftId
 * @returns {Promise<Spacecraft>}
 */
const deleteSpacecraftById = async (spacecraftId) => {
  const spacecraft = await getSpacecraftById(spacecraftId);
  if (!spacecraft) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Spacecraft not found');
  }
  await spacecraft.remove();
  return spacecraft;
};

module.exports = {
  createSpacecraft,
  querySpacecrafts,
  getSpacecraftById,
  updateSpacecraftById,
  deleteSpacecraftById,
};
