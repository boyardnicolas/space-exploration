const httpStatus = require('http-status');
const { Mission } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a mission
 * @param {Object} missionBody
 * @returns {Promise<Mission>}
 */
const createMission = async (missionBody) => {
  if (await Mission.isNameTaken(missionBody.name)) {
    throw new ApiError(httpStatus.CONFLICT, 'Name already taken');
  }
  return Mission.create(missionBody);
};

/**
 * Query for missions
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryMissions = async (filter, options) => {
  const missions = await Mission.paginate(filter, options);
  return missions;
};

/**
 * Get mission by id
 * @param {ObjectId} id
 * @returns {Promise<Mission>}
 */
const getMissionById = async (id) => {
  return Mission.findById(id);
};

/**
 * Update mission by id
 * @param {ObjectId} missionId
 * @param {Object} updateBody
 * @returns {Promise<Mission>}
 */
const updateMissionById = async (missionId, updateBody) => {
  const mission = await getMissionById(missionId);
  if (!mission) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Mission not found');
  }
  if (await Mission.isNameTaken(updateBody.name)) {
    throw new ApiError(httpStatus.CONFLICT, 'Name already taken');
  }
  Object.assign(mission, updateBody);
  await mission.save();
  return mission;
};

/**
 * Delete mission by id
 * @param {ObjectId} missionId
 * @returns {Promise<Mission>}
 */
const deleteMissionById = async (missionId) => {
  const mission = await getMissionById(missionId);
  if (!mission) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Mission not found');
  }
  await mission.remove();
  return mission;
};

module.exports = {
  createMission,
  queryMissions,
  getMissionById,
  updateMissionById,
  deleteMissionById,
};
