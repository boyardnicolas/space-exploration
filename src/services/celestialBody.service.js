const httpStatus = require('http-status');
const { CelestialBody } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a celestial body
 * @param {Object} celestialBodyBody
 * @returns {Promise<CelestialBody>}
 */
const createCelestialBody = async (celestialBodyBody) => {
  if (await CelestialBody.isNameTaken(celestialBodyBody.name)) {
    throw new ApiError(httpStatus.CONFLICT, 'Name already taken');
  }
  return CelestialBody.create(celestialBodyBody);
};

/**
 * Query for celestialBodies
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryCelestialBodies = async (filter, options) => {
  const celestialBodies = await CelestialBody.paginate(filter, options);
  return celestialBodies;
};

/**
 * Get celestial body by id
 * @param {ObjectId} id
 * @returns {Promise<CelestialBody>}
 */
const getCelestialBodyById = async (id) => {
  return CelestialBody.findById(id);
};

/**
 * Update celestial body by id
 * @param {ObjectId} celestialBodyId
 * @param {Object} updateBody
 * @returns {Promise<CelestialBody>}
 */
const updateCelestialBodyById = async (celestialBodyId, updateBody) => {
  const celestialBody = await getCelestialBodyById(celestialBodyId);
  if (!celestialBody) {
    throw new ApiError(httpStatus.NOT_FOUND, 'CelestialBody not found');
  }
  if (await CelestialBody.isNameTaken(updateBody.name)) {
    throw new ApiError(httpStatus.CONFLICT, 'Name already taken');
  }
  Object.assign(celestialBody, updateBody);
  await celestialBody.save();
  return celestialBody;
};

/**
 * Delete celestial body by id
 * @param {ObjectId} celestialBodyId
 * @returns {Promise<CelestialBody>}
 */
const deleteCelestialBodyById = async (celestialBodyId) => {
  const celestialBody = await getCelestialBodyById(celestialBodyId);
  if (!celestialBody) {
    throw new ApiError(httpStatus.NOT_FOUND, 'CelestialBody not found');
  }
  await celestialBody.remove();
  return celestialBody;
};

module.exports = {
  createCelestialBody,
  queryCelestialBodies,
  getCelestialBodyById,
  updateCelestialBodyById,
  deleteCelestialBodyById,
};
