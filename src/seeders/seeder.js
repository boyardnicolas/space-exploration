const mongoose = require('mongoose');
const { userService } = require('../services');
const config = require('../config/config');
const logger = require('../config/logger');
const { Mission, Spacecraft } = require('../models');

mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
  logger.info('Connected to MongoDB');
  const promises = [];
  promises.push(Mission.createCollection());
  promises.push(Spacecraft.createCollection());
  promises.push(
    userService
      .createUser({
        name: 'admin',
        email: 'admin@demo.demo',
        password: 'demodemo1',
        role: 'admin',
      })
      .then(() => logger.info('User admin@demo.demo / demodemo1 created'))
  );
  promises.push(
    userService
      .createUser({
        name: 'user',
        email: 'user@demo.demo',
        password: 'demodemo1',
        role: 'user',
      })
      .then(() => logger.info('User user@demo.demo / demodemo1 created'))
  );
  Promise.all(promises).then(process.exit);
});
