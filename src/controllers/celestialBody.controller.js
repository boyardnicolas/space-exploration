const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { celestialBodyService } = require('../services');

const createCelestialBody = catchAsync(async (req, res) => {
  const celestialBody = await celestialBodyService.createCelestialBody(req.body);
  res.status(httpStatus.CREATED).send(celestialBody);
});

const getCelestialBodies = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await celestialBodyService.queryCelestialBodies(filter, options);
  res.send(result);
});

const getCelestialBody = catchAsync(async (req, res) => {
  const celestialBody = await celestialBodyService.getCelestialBodyById(req.params.celestialBodyId);
  if (!celestialBody) {
    throw new ApiError(httpStatus.NOT_FOUND, 'CelestialBody not found');
  }
  res.send(celestialBody);
});

const updateCelestialBody = catchAsync(async (req, res) => {
  const celestialBody = await celestialBodyService.updateCelestialBodyById(req.params.celestialBodyId, req.body);
  res.send(celestialBody);
});

const deleteCelestialBody = catchAsync(async (req, res) => {
  await celestialBodyService.deleteCelestialBodyById(req.params.celestialBodyId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createCelestialBody,
  getCelestialBodies,
  getCelestialBody,
  updateCelestialBody,
  deleteCelestialBody,
};
