const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { missionService, spacecraftService } = require('../services');

const createMission = catchAsync(async (req, res) => {
  if (req.body.spacecraft) {
    const spacecraft = await spacecraftService.getSpacecraftById(req.body.spacecraft);
    if (!spacecraft) {
      throw new ApiError(httpStatus.NOT_FOUND, 'Spacecraft not found');
    }
  }
  const mission = await missionService.createMission(req.body);
  res.status(httpStatus.CREATED).send(mission);
});

const getMissions = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await missionService.queryMissions(filter, options);
  res.send(result);
});

const getMission = catchAsync(async (req, res) => {
  const mission = await missionService.getMissionById(req.params.missionId);
  if (!mission) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Mission not found');
  }
  await mission.populate('spacecraft').execPopulate();
  res.send(mission);
});

const updateMission = catchAsync(async (req, res) => {
  const mission = await missionService.updateMissionById(req.params.missionId, req.body);
  res.send(mission);
});

const deleteMission = catchAsync(async (req, res) => {
  await missionService.deleteMissionById(req.params.missionId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createMission,
  getMissions,
  getMission,
  updateMission,
  deleteMission,
};
