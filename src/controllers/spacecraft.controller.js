const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { spacecraftService } = require('../services');
const { Spacecraft } = require('../models');

const createSpacecraft = catchAsync(async (req, res) => {
  const spacecraft = await spacecraftService.createSpacecraft(req.body);
  res.status(httpStatus.CREATED).send(spacecraft);
});

const getSpacecrafts = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await spacecraftService.querySpacecrafts(filter, options);
  res.send(result);
});

const getSpacecraft = catchAsync(async (req, res) => {
  const spacecraft = await Spacecraft.find({ _id: req.params.spacecraftId }).populate('mission');
  if (!spacecraft) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Spacecraft not found');
  }
  res.send(spacecraft);
});

const updateSpacecraft = catchAsync(async (req, res) => {
  const spacecraft = await spacecraftService.updateSpacecraftById(req.params.spacecraftId, req.body);
  res.send(spacecraft);
});

const deleteSpacecraft = catchAsync(async (req, res) => {
  await spacecraftService.deleteSpacecraftById(req.params.spacecraftId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createSpacecraft,
  getSpacecrafts,
  getSpacecraft,
  updateSpacecraft,
  deleteSpacecraft,
};
